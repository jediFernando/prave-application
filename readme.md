#Some setup information :).

##Spring Security Setup
insert into AccessGroup(description,name) values ('GRUPO DE GESTORES' , 'GESTOR');
insert into AccessGroup(description,name) values ('GRUPO DE GERENTES' , 'GERENTE');
insert into AccessGroup(description,name) values ('GRUPO DE ESTOQUISTAS' , 'ESTOQUE');
insert into AccessGroup(description,name) values ('GRUPO DE VENDEDORES' , 'VENDEDOR');
insert into AccessGroup(description,name) values ('GRUPO DE GERENTES E VENDEDORES' , 'GERENTE E VENDEDOR');

insert into Permission(name) values ('GESTOR');
insert into Permission(name) values ('GERENTE');
insert into Permission(name) values ('ESTOQUE');
insert into Permission(name) values ('VENDEDOR');
insert into Permission(name) values ('GERENTE E VENDEDOR');

insert into AccessGroup_Permission(accessgroups_id, permissions_id) values (1,1);
insert into AccessGroup_Permission(accessgroups_id, permissions_id) values (2,2);
insert into AccessGroup_Permission(accessgroups_id, permissions_id) values (3,3);
insert into AccessGroup_Permission(accessgroups_id, permissions_id) values (4,4);
insert into AccessGroup_Permission(accessgroups_id, permissions_id) values (5,5);

