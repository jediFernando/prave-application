
function getRodadas(id) {
	var url = $('#getRodadas').val() + id;
	$
			.get(
					url,
					function(data, status) {
						if (status == "success" && data.rodadas.length > 0
								&& data.equipes.length > 0) {
							if (sessionStorage.hasOwnProperty('allEquipes')) {
								sessionStorage.removeItem('allEquipes')
							}
							sessionStorage.setItem('allEquipes', JSON
									.stringify(data.equipes))
							setSelect(data);
						} else if (data.rodadas.length == 0) {

							swal(
									{
										title : "Todas as rodadas deste campeonato já estão preenchidas!",
										text : ""
									}, function() {
										resetSelectEquipes();
									});
						}
					});
}

function setSelect(data) {

	var htmlRodadas = '<option value="">Rodadas</option>';

	var htmlEquipes = '<div class="row"><div class="col-lg-3"><span>Mandante</span></div>'
			+ '<div class="col-lg-3"><span>Visitante</span></div>'
			+ '<div class="col-lg-3"><span>Data partida</span></div>'
			+ '<div class="col-lg-3"><span>Local partida</span></div></div>';

	var selectEquipes = '';
	var locaisPartida = '';

	$.each(data.rodadas, function(i, x) {
		htmlRodadas += '<option value="' + x.id + '">' + x.numeroRodada
				+ 'ª Rodada</option>';
	});

	$.each(data.equipes,
			function(i, x) {
				selectEquipes += '<option value="' + x.id + '">' + x.nome
						+ '</option>';
			});

	$.each(data.locaisPartida, function(i, x) {
		locaisPartida += '<option value="' + x.id + '">' + x.nomeLocalPartida
				+ '</option>';
	});

	htmlEquipes += '<div class="col-lg-6"><div class="col-lg-6">'
			+ '<div class="form-group">'
			+ '<select class="form-control selectEquipes" id="0"><option value=""></option>'
			+ selectEquipes + '</select>' + '</div>' + '</div>';

	var htmlDataELocal = '<div class="col-lg-6">';


	for (var i = 0; i < data.equipes.length -1; i++) {
		var id = i + 1;

		htmlEquipes += '<div class="col-lg-6">' + '<div class="form-group">'
				+ '<select class="form-control selectEquipes" id="' + id + '">'
				+ '<option value=""><option>' + '</select>' + '</div>'
				+ '</div>';
		if (id % 2 != 0) {
			htmlDataELocal += '<div class="col-lg-6">'
					+ '<div class="form-group">'
					+ '<input type="text" class="form-control varzeafc-date" id="dataPartida'
					+ i + '">' + '</div>' + '</div>' + '<div class="col-lg-6">'
					+ '<div class="form-group">'
					+ '<select class="form-control" id="localPartida' + i
					+ '"><option value=""></option>' + locaisPartida
					+ '</select>' + '</div>' + '</div>';
		}

	}

	htmlEquipes += '</div>';
	htmlDataELocal += '</div>';

	var htmlTotal = htmlEquipes + htmlDataELocal;

	$('#selectRodada').html(htmlRodadas);
	$('#jogos').html(htmlTotal);

	$(".selectEquipes").on("change", function() {
		if ($(this).val() != "") {
			helperSelectEquipes($(this).val(), this.id);
		}
	});

	$(".varzeafc-date").mask("99/99/9999");

	$('.varzeafc-date').datepicker({
		format : "dd/mm/yyyy",
		language : "pt-BR",
		startDate : "dateToday"
	});

}

function helperSelectEquipes(id, anterior) {
	var proximo = parseInt(anterior) + 1;
	var equipes;

	if (!sessionStorage.hasOwnProperty('filterEquipes')) {
		equipes = JSON.parse(sessionStorage.getItem('allEquipes'));
		sessionStorage.setItem('filterEquipes', equipes);
	} else {
		equipes = JSON.parse(sessionStorage.getItem('filterEquipes'));
	}

	var equipesRestantes = equipes.filter(function(x) {
		return x.id != id;
	})

	var selectEquipes = '<option value=""></option>';

	$.each(equipesRestantes,
			function(i, x) {
				selectEquipes += '<option value="' + x.id + '">' + x.nome
						+ '</option>';
			});

	$('#' + proximo.toString()).html(selectEquipes);
	$('#' + anterior).prop('disabled', 'disabled');

	sessionStorage.removeItem('filterEquipes');
	sessionStorage.setItem('filterEquipes', JSON.stringify(equipesRestantes))

}

function resetSelectEquipes() {
	sessionStorage.clear();
	location.reload();
}

function save() {

	if (validaPreenchimento()) {

		var jogos = [];
		var confrontos = {};
		var obj = {
			"id" : $('#selectRodada').val(),
			"jogos" : "",
			"campeonato" : {
				"id" : $('#selectCampeonato').val()
			}
		}

		var selectConfrontos = $('.selectEquipes');

		$.each(selectConfrontos, function(x, i) {
			if (i.id % 2 == 0) {
				confrontos.equipeMandante = {
					"id" : i.value
				}
				confrontos.dataDaPartida = $('#dataPartida' + i.id).val();
				confrontos.localDaPartida = {
					"id" : $('#localPartida' + i.id).val()
				}
			} else {
				confrontos.equipeVisitante = {
					"id" : i.value
				}
				jogos.push(confrontos);
				confrontos = {};
			}
		});

		obj.jogos = jogos;
		var url = $('#addConfrontos').val()

		$.ajax({
			url : url,
			type : "post",
			data : JSON.stringify(obj),
			contentType : "application/json",
			dataType : "json",
			success : function(response) {
				if (response.status === "Ok") {
					swal({
						title : "Rodada cadastrada com sucesso",
						text : "",
						type : "success"
					}, function() {
						resetSelectEquipes();
					});
				} else {
					swal({
						title : "Ocorreu um erro interno",
						text : "",
						type : "error"
					}, function() {
						resetSelectEquipes();
					});
				}

			},
			error : function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}

		});

	} else {
		swal("Preencha todos os campos!")
	}

}

function validaPreenchimento() {
	var errors = 0;

	$('select').each(function(i) {
		if (this.value == "") {
			errors++;
		}
	})

	$('input:text').each(function(i) {
		if (this.value == "") {
			errors++;
		}
	})

	if (errors) {
		errors = 0;
		return false;
	} else {
		return true;
	}

}

$(function() {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
});


function getJogosRodadas(id) {
	var url = $('#getJogos')[0].href;
	var urlFinal = url+id;
	$('#getJogos')[0].href = urlFinal;
	$('#getJogos')[0].click();
	
}
