package br.com.prave.models;

import br.com.prave.enums.ProviderStatus;
import br.com.prave.validation.Cnpj;
import br.com.prave.validation.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.List;

@Entity
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message= "Nome é obrigatório")
    private String name;
    @NotBlank(message= "CNPJ é obrigatório")
    @Cnpj
    private String document;
    @NotBlank(message= "E-mail é obrigatório")
    @Email
    private String email;
    private String address;
    private String phoneNumber;
    @ManyToOne
    private Company company;
    @Enumerated(EnumType.STRING)
    private ProviderStatus providerStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ProviderStatus getProviderStatus() {
        return providerStatus;
    }

    public void setProviderStatus(ProviderStatus providerStatus) {
        this.providerStatus = providerStatus;
    }
}
