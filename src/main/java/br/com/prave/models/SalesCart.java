package br.com.prave.models;

import br.com.prave.daos.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SalesCart {

    private List<ShoppingProduct> shoppingProducts = new ArrayList<>();

    public void addToCart(Product product) {
        boolean exist = false;
        for (int i = 0; i < shoppingProducts.size(); i++) {
            if (shoppingProducts.get(i).getProduct().getId() == product.getId()) {
                exist = true;
                shoppingProducts.get(i).getProduct().setQuantity(shoppingProducts.get(i).getProduct().getQuantity() + 1);
                shoppingProducts.get(i).setSubTotal(shoppingProducts.get(i).getSubTotal() + product.getPrice());
            }
        }
        if (!exist) {
            product.setQuantity(1);
            ShoppingProduct shoppingProduct = new ShoppingProduct();
            shoppingProduct.setProduct(product);
            shoppingProduct.setSubTotal(product.getPrice());
            shoppingProducts.add(shoppingProduct);
        }
    }

    public void updateQuantity(ProductDAO productDAO) {
        for (ShoppingProduct shoppingProduct : shoppingProducts) {
            Product productUpdate = productDAO.findById(shoppingProduct.getProduct().getId());
            productUpdate.setQuantity(productUpdate.getQuantity() - shoppingProduct.getProduct().getQuantity());
            productDAO.update(productUpdate);
        }
    }

    public void updateProduct(Product product) {
        for (int i = 0; i < shoppingProducts.size(); i++) {
            if (shoppingProducts.get(i).getProduct().getId() == product.getId()) {
                ShoppingProduct shoppingProduct = new ShoppingProduct();
                shoppingProduct.setProduct(product);
                shoppingProduct.setSubTotal(product.getPrice() * product.getQuantity());
                shoppingProducts.set(i, shoppingProduct);
            }
        }
    }

    public void deleteProduct(Integer id) {
        for (int i = 0; i < shoppingProducts.size(); i++) {
            if (shoppingProducts.get(i).getProduct().getId() == id) {
                shoppingProducts.remove(i);
            }
        }
    }

    public List<ShoppingProduct> getShoppingProducts() {
        return shoppingProducts;
    }

    public void setShoppingProducts(List<ShoppingProduct> shoppingProducts) {
        this.shoppingProducts = shoppingProducts;
    }
}
