package br.com.prave.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import br.com.prave.conf.UserSystem;
import br.com.prave.enums.AccessStatus;
import br.com.prave.validation.Login;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Entity
public class AccessUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Login é obrigatório")
    @Column(name = "login", unique = true)
    @Login(message = "Login já existe")
    private String login;
    @NotBlank(message = "Senha é obrigatória")
    private String password;

    private AccessStatus accessStatus;

    @ManyToMany
    private List<AccessGroup> accessGroups;

    @ManyToMany
    private List<Permission> permissions;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccessStatus getAccessStatus() {
        return accessStatus;
    }

    public void setAccessStatus(AccessStatus accessStatus) {
        this.accessStatus = accessStatus;
    }

    public List<AccessGroup> getAccessGroups() {
        return accessGroups;
    }

    public void setAccessGroups(List<AccessGroup> accessGroups) {
        this.accessGroups = accessGroups;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AccessUser other = (AccessUser) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "AccessUser [id=" + id + ", login=" + login + "]";
    }

    public String cryptPassword(String password) {
        return new BCryptPasswordEncoder().encode(password);
    }

    public UserSystem getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserSystem user = (UserSystem) auth.getPrincipal();
        return user;
    }
}