package br.com.prave.models;

import br.com.prave.validation.Cpf;
import org.hibernate.validator.constraints.NotBlank;
import br.com.prave.validation.Email;

import javax.persistence.*;

@Entity
public class Employee extends AccessUser {

    @NotBlank(message= "Nome é obrigatório")
    private String name;
    @NotBlank(message= "CPF é obrigatório")
    @Cpf
    private String document;
    @NotBlank(message= "E-mail é obrigatório")
    @Email
    private String email;

    @ManyToOne
    private Company company;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
