package br.com.prave.models;

import br.com.prave.enums.BuyOrderStatus;

import javax.persistence.*;

@Entity
public class BuyOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer quantity;
    @Enumerated(EnumType.STRING)
    private BuyOrderStatus buyOrderStatus;
    @ManyToOne
    private Product product;
    @ManyToOne
    private Provider provider;
    @ManyToOne
    private Company company;

    public Integer getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BuyOrderStatus getBuyOrderStatus() {
        return buyOrderStatus;
    }

    public void setBuyOrderStatus(BuyOrderStatus buyOrderStatus) {
        this.buyOrderStatus = buyOrderStatus;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
