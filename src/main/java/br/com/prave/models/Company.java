package br.com.prave.models;

import br.com.prave.validation.Cnpj;
import br.com.prave.validation.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.List;

@Entity
public class Company extends AccessUser {

    @NotBlank(message= "Nome é obrigatório")
    private String name;
    @NotBlank(message= "CNPJ é obrigatório")
    @Cnpj
    private String document;
    @NotBlank(message= "Endereço é obrigatório")
    private String address;
    @NotBlank(message= "E-mail é obrigatório")
    @Email
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
