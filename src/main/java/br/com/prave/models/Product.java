package br.com.prave.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

import br.com.prave.conf.UserSystem;
import br.com.prave.daos.EmployeeDAO;
import br.com.prave.enums.ProductStatus;
import br.com.prave.enums.WeightType;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Entity
public class Product {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;

   @NotBlank(message= "Nome é obrigatório")
   private String name;

   private String description;
   private String color;
   private String model;
   private String temperature;
   private String weight;
   private String storage;
   private String size;
   private String manufacturer;
   private Integer quantity;
   private Integer minimum;

   @NotNull(message= "Preço é obrigatório")
   private Long price;

   @Enumerated(EnumType.STRING)
   private WeightType weightType;

   @Enumerated(EnumType.STRING)
   private ProductStatus productStatus;

   @ManyToOne
   private Company company;

   public UserSystem getUser() {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      UserSystem user = (UserSystem) auth.getPrincipal();
      return user;
   }

   public Integer getId()
   {
      return this.id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getColor() {
      return color;
   }

   public void setColor(String color) {
      this.color = color;
   }

   public String getModel() {
      return model;
   }

   public void setModel(String model) {
      this.model = model;
   }

   public String getTemperature() {
      return temperature;
   }

   public void setTemperature(String temperature) {
      this.temperature = temperature;
   }

   public String getWeight() {
      return weight;
   }

   public void setWeight(String weight) {
      this.weight = weight;
   }

   public WeightType getWeightType() {
      return weightType;
   }

   public void setWeightType(WeightType weightType) {
      this.weightType = weightType;
   }

   public String getStorage() {
      return storage;
   }

   public void setStorage(String storage) {
      this.storage = storage;
   }

   public String getSize() {
      return size;
   }

   public void setSize(String size) {
      this.size = size;
   }

   public String getManufacturer() {
      return manufacturer;
   }

   public void setManufacturer(String manufacturer) {
      this.manufacturer = manufacturer;
   }

   public Integer getQuantity() {
      return quantity;
   }

   public void setQuantity(Integer quantity) {
      this.quantity = quantity;
   }

   public Integer getMinimum() {
      return minimum;
   }

   public void setMinimum(Integer minimum) {
      this.minimum = minimum;
   }

   public Long getPrice() {
      return price;
   }

   public void setPrice(Long price) {
      this.price = price;
   }

   public ProductStatus getProductStatus() {
      return productStatus;
   }

   public void setProductStatus(ProductStatus productStatus) {
      this.productStatus = productStatus;
   }

   public Company getCompany() {
      return company;
   }

   public void setCompany(Company company) {
      this.company = company;
   }
}
