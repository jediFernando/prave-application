package br.com.prave.enums;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

public enum AccessStatus {

    ACTIVE("Ativo"),
    INACTIVE("Inativo");

    private String description;

    public String getDescription() {
        return description;
    }

    AccessStatus(String description) {
        this.description = description;
    }

}
