package br.com.prave.enums;

public enum BuyOrderStatus {

    WAITING("Aguardando"),
    APPROVED("Aprovado");

    private String description;

    public String getDescription() {
        return description;
    }

    BuyOrderStatus(String description) {
        this.description = description;
    }
}
