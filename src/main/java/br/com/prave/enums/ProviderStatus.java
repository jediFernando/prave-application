package br.com.prave.enums;

public enum ProviderStatus {

    ACTIVE("Ativo"),
    INACTIVE("Inativo");

    private String description;

    public String getDescription() {
        return description;
    }

    ProviderStatus(String description) {
        this.description = description;
    }

}
