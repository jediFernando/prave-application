package br.com.prave.enums;

public enum WeightType {

    NONE(""),
    ML("mL"),
    L("L"),
    GR("Gr"),
    KG("Kg");

    private String description;

    public String getDescription() {
        return description;
    }

    WeightType(String description) {
        this.description = description;
    }
}
