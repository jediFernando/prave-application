package br.com.prave.enums;

public enum ProductStatus {

    ACTIVE("Disponível"),
    UNRELEASED("Não liberado para venda");

    private String description;

    public String getDescription() {
        return description;
    }

    ProductStatus(String description) {
        this.description = description;
    }
}
