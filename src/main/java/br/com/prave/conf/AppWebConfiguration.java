package br.com.prave.conf;

import br.com.prave.validation.GroupFormatter;
import br.com.prave.validation.ProductFormatter;
import br.com.prave.validation.ProviderFormatter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "br.com.prave")
public class AppWebConfiguration extends WebMvcConfigurerAdapter
{

   @Override
   public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
   {
      configurer.enable();
   }

   @Override
   public void addFormatters(FormatterRegistry registry) {
      registry.addFormatter(new GroupFormatter());
      registry.addFormatter(new ProductFormatter());
      registry.addFormatter(new ProviderFormatter());
   }

   @Bean
   public InternalResourceViewResolver internalResourceViewResolver() {
      InternalResourceViewResolver resolver = new InternalResourceViewResolver();
      resolver.setPrefix("/WEB-INF/views/");
      resolver.setSuffix(".html");
      return resolver;
   }
}
