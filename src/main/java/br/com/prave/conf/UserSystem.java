package br.com.prave.conf;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UserSystem extends User {

    private static final long serialVersionUID = 1L;
    private Integer id;

    public UserSystem(String login, String password, Collection<? extends GrantedAuthority> authorities, Integer id) {
        super(login, password, authorities);
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}