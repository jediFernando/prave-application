package br.com.prave.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

   @Override
   protected void configure(HttpSecurity http) throws Exception {

      http.authorizeRequests()
              .antMatchers("/", "/cadastro").permitAll()
              .antMatchers("/dashboard").hasAnyRole("GESTOR", "GERENTE", "ESTOQUE", "VENDEDOR", "GERENTE E VENDEDOR")
              .antMatchers("/dashboard/employee/**").hasAnyRole("GESTOR")
              .antMatchers("/dashboard/reports/**").hasAnyRole("GERENTE", "GERENTE E VENDEDOR")
              .antMatchers("/dashboard/stock/**").hasAnyRole("ESTOQUE")
              .antMatchers("/dashboard/sell/**").hasAnyRole("VENDEDOR", "GERENTE E VENDEDOR")
              .anyRequest().authenticated().and()
              .formLogin().loginPage("/login").defaultSuccessUrl("/dashboard").permitAll()
              .and()
              .logout().deleteCookies("JSESSIONID").logoutSuccessUrl("/");
   }

   @Autowired
   private UserDetailsServiceLogin users;

   @Override
   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.userDetailsService(users).passwordEncoder(new BCryptPasswordEncoder());
   }

   @Override
   public void configure(WebSecurity web) throws Exception {
      web.ignoring().antMatchers("/assets/**");
   }
}
