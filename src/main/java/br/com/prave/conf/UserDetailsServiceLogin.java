package br.com.prave.conf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.prave.daos.AccessGroupDAO;
import br.com.prave.daos.AccessUserDAO;
import br.com.prave.daos.PermissionDAO;
import br.com.prave.models.AccessGroup;
import br.com.prave.enums.AccessStatus;
import br.com.prave.models.AccessUser;
import br.com.prave.models.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsServiceLogin implements UserDetailsService {

    @Autowired
    private AccessUserDAO accessUserDAO;

    @Autowired
    private AccessGroupDAO accessGroupDAO;

    @Autowired
    private PermissionDAO permissionDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AccessUser accessUser = accessUserDAO.searchLogin(username);
        if (accessUser == null) {
            throw new UsernameNotFoundException("Usuário não encontrado!");
        }

        // TODO: Verificar se ta mostrando a mensagem de erro certa ao logar.
        if (accessUser.getAccessStatus() == AccessStatus.INACTIVE) {
            throw new UsernameNotFoundException("Usuário inativado. Entre em contato com o Gestor da sua empresa.");
        }

        return new UserSystem(accessUser.getLogin(), accessUser.getPassword(), authorities(accessUser), accessUser.getId());
    }

    public Collection<? extends GrantedAuthority> authorities(AccessUser accessUser) {
        return authorities(accessGroupDAO.searchUserGroup(accessUser));
    }

    public Collection<? extends GrantedAuthority> authorities(List<AccessGroup> accessGroups) {
        Collection<GrantedAuthority> auths = new ArrayList<>();

        for (AccessGroup accessGroup : accessGroups) {
            List<Permission> list = permissionDAO.searchGroupPermission(accessGroup);
            for (Permission permission : list) {
                System.out.println(permission.getName());
                auths.add(new SimpleGrantedAuthority("ROLE_" + permission.getName()));
            }
        }

        return auths;
    }
}
