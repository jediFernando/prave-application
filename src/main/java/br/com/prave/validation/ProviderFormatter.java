package br.com.prave.validation;

import br.com.prave.models.Provider;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class ProviderFormatter implements Formatter<Provider> {

    @Override
    public String print(Provider provider, Locale locale) {
        return provider.getId().toString();
    }

    @Override
    public Provider parse(String id, Locale locale) throws ParseException {
        Provider provider = new Provider();
        provider.setId(Integer.parseInt(id));
        return provider;
    }
}
