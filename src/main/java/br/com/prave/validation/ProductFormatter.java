package br.com.prave.validation;

import br.com.prave.models.Product;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class ProductFormatter implements Formatter<Product> {

    @Override
    public String print(Product product, Locale locale) {
        return product.getId().toString();
    }

    @Override
    public Product parse(String id, Locale locale) throws ParseException {
        Product product = new Product();
        product.setId(Integer.parseInt(id));
        return product;
    }
}
