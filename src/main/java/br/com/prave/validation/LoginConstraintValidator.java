package br.com.prave.validation;

import br.com.prave.daos.AccessUserDAO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LoginConstraintValidator  implements ConstraintValidator<Login, String> {

    @Autowired
    private AccessUserDAO accessUserDAO;

    @Override
    public void initialize(Login login) {}

    @Override
    public boolean isValid(String login, ConstraintValidatorContext arg1) {


        if (!login.trim().isEmpty() && accessUserDAO != null && accessUserDAO.searchLogin(login) != null)
            return false;

        return true;

    }

}
