package br.com.prave.validation;

import br.com.prave.models.AccessGroup;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class GroupFormatter implements Formatter<AccessGroup> {

    @Override
    public String print(AccessGroup group, Locale locale) {
        return group.getId().toString();
    }

    @Override
    public AccessGroup parse(String id, Locale locale) throws ParseException {
        AccessGroup group = new AccessGroup();
        group.setId(Integer.parseInt(id));
        return group;
    }
}
