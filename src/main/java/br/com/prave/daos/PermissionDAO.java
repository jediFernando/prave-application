package br.com.prave.daos;

import br.com.prave.models.AccessGroup;
import br.com.prave.models.Permission;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class PermissionDAO  {

    @PersistenceContext
    private EntityManager manager;

    public List<Permission> searchGroupPermission(AccessGroup accessGroup) {
        return manager.createQuery("Select p from Permission p JOIN p.accessGroups accessGroup where accessGroup.id = :i", Permission.class)
                .setParameter("i", accessGroup.getId()).getResultList();
    }
}
