package br.com.prave.daos;

import br.com.prave.models.AccessUser;
import br.com.prave.models.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository
@Transactional
public class AccessUserDAO {

    @PersistenceContext
    private EntityManager manager;

    public void save(AccessUser accessUser){
        manager.persist(accessUser);

    }

    public AccessUser searchLogin(String login) {
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<AccessUser> criteria = builder.createQuery(AccessUser.class);
            Root<AccessUser> root = criteria.from(AccessUser.class);
            Predicate like = builder.like(root.<String>get("login"), login);
            return manager.createQuery(criteria.select(root).where(like)).getSingleResult();

        } catch (NoResultException e) {
            return null;
        }
    }

    public Employee findById(int id) {
        return manager.createQuery("select u from Employee u where u.id = :id", Employee.class)
                .setParameter("id", id)
                .getSingleResult();
    }

}
