package br.com.prave.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import br.com.prave.models.PaginatedList;
import br.com.prave.models.Product;

@Repository
public class ProductDAO {

   @PersistenceContext
   private EntityManager manager;

   public List<Product> allQuantity(int id) {
      return manager.createQuery("select p from Product p where p.company.id = :id and p.quantity > 0 order by p.name", Product.class)
              .setParameter("id", id)
              .getResultList();
   }

   public List<Product> all(int id) {
      return manager.createQuery("select p from Product p where p.company.id = :id order by p.name", Product.class)
              .setParameter("id", id)
              .getResultList();
   }

   public List<Product> listAll() {
      return manager.createQuery("select p from Product p", Product.class)
              .getResultList();
   }

   public void save(Product product) {
      manager.persist(product);
   }

   public Product findById(Integer id)
   {
      return manager.find(Product.class, id);
   }

   public void remove(Product product)
   {
      manager.remove(product);
   }

   public void update(Product product) {
      manager.merge(product);
   }

   public PaginatedList paginated(int page, int max) {
      return new PaginatorQueryHelper().list(manager, Product.class, page, max);
   }

}
