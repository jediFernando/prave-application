package br.com.prave.daos;

import br.com.prave.models.BuyOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class OrderDAO {

    @PersistenceContext
    private EntityManager manager;
    public List<BuyOrder> all(int id) {
        return manager.createQuery("select o from BuyOrder o where o.company.id = :id and o.buyOrderStatus = 'WAITING'", BuyOrder.class)
                .setParameter("id", id)
                .getResultList();
    }

    public List<BuyOrder> listAll() {
        return manager.createQuery("select o from BuyOrder o", BuyOrder.class)
                .getResultList();
    }

    public void save(BuyOrder buyOrder) {
        manager.persist(buyOrder);
    }

    public BuyOrder findById(Integer id){
        return manager.find(BuyOrder.class, id);
    }

    public void remove(BuyOrder buyOrder) {
        manager.remove(buyOrder);
    }

    public void update(BuyOrder buyOrder) {
        manager.merge(buyOrder);
    }

}
