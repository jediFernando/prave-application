package br.com.prave.daos;

import br.com.prave.models.AccessUser;
import br.com.prave.models.Employee;
import br.com.prave.models.PaginatedList;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EmployeeDAO {

    @PersistenceContext
    private EntityManager manager;

    public List<Employee> all(int id) {
        return manager.createQuery("select e from Employee e where e.company.id = :id", Employee.class)
                .setParameter("id", id)
                .getResultList();
    }

    public void save(Employee employee)
    {
        manager.persist(employee);
    }

    public Employee findById(Integer id)
    {
        return manager.find(Employee.class, id);
    }

    public Employee findByLogin(String login) {
        return manager.find(Employee.class, login);
    }

    public void remove(Employee employee)
    {
        manager.remove(employee);
    }

    public void update(Employee employee) {
        manager.merge(employee);
    }

    public boolean findByDocument(String document) {
        return manager.find(Employee.class, document) != null;
    }

    public PaginatedList paginated(int page, int max) {
        return new PaginatorQueryHelper().list(manager, Employee.class, page, max);
    }
}
