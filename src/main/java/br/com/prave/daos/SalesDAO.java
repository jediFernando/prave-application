package br.com.prave.daos;

import br.com.prave.models.Sales;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class SalesDAO {

    @PersistenceContext
    private EntityManager manager;
    public List<Sales> all(int id) {
        return manager.createQuery("select s from Sales s where s.company.id = :id order by s.name", Sales.class)
                .setParameter("id", id)
                .getResultList();
    }

    public List<Sales> listAll() {
        return manager.createQuery("select s from Sales s", Sales.class)
                .getResultList();
    }

    public void save(Sales sales) {
        manager.persist(sales);
    }

    public Sales findById(Integer id)
    {
        return manager.find(Sales.class, id);
    }

    public void remove(Sales sales)
    {
        manager.remove(sales);
    }

    public void update(Sales sales) {
        manager.merge(sales);
    }

}
