package br.com.prave.daos;

import br.com.prave.models.PaginatedList;
import br.com.prave.models.Provider;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ProviderDAO {

    @PersistenceContext
    private EntityManager manager;

    public List<Provider> all(int id) {
        return manager.createQuery("select prov from Provider prov where prov.company.id = :id order by prov.name", Provider.class)
                .setParameter("id", id)
                .getResultList();
    }

    public List<Provider> allActive(int id) {
        return manager.createQuery("select prov from Provider prov where prov.company.id = :id and prov.providerStatus = 'ACTIVE' order by prov.name", Provider.class)
                .setParameter("id", id)
                .getResultList();
    }


    public List<Provider> listAll() {
        return manager.createQuery("select prov from Provider prov", Provider.class)
                .getResultList();
    }

    public void save(Provider provider) {
        manager.persist(provider);
    }

    public Provider findById(Integer id)
    {
        return manager.find(Provider.class, id);
    }

    public void remove(Provider provider)
    {
        manager.remove(provider);
    }

    public void update(Provider provider) {
        manager.merge(provider);
    }

    public PaginatedList paginated(int page, int max) {
        return new PaginatorQueryHelper().list(manager, Provider.class, page, max);
    }

}
