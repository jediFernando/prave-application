package br.com.prave.daos;

import br.com.prave.enums.AccessStatus;
import br.com.prave.models.AccessUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class AccessStatusDAO {

    @PersistenceContext
    private EntityManager manager;

    public List<AccessStatus> searchUserStatus(AccessUser accessUser) {
        return manager.createQuery("select s from AccessStatus s JOIN s.accessUsers accessUser where accessUser.id = :i", AccessStatus.class)
                .setParameter("i", accessUser.getId())
                .getResultList();
    }

    public List<AccessStatus> all() {
        return manager.createQuery("select s from AccessStatus s where s.id != 0", AccessStatus.class).getResultList();
    }
}
