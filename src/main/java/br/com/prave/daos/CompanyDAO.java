package br.com.prave.daos;

import br.com.prave.models.Company;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.ResultSet;
import java.util.List;

@Repository
public class CompanyDAO {

    @PersistenceContext
    private EntityManager manager;

    public List<Company> all() {
        return manager.createQuery("select c from Company c", Company.class).getResultList();
    }

    public void save(Company company)
    {
        manager.persist(company);
    }

    public Company findById(Integer id) {
        return manager.find(Company.class, id);
    }

    public void remove(Company company)
    {
        manager.remove(company);
    }

    public void update(Company company)
    {
        manager.merge(company);
    }
}
