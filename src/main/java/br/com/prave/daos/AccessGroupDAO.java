package br.com.prave.daos;

import br.com.prave.models.AccessGroup;
import br.com.prave.models.AccessUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class AccessGroupDAO {

    @PersistenceContext
    private EntityManager manager;

    public List<AccessGroup> searchUserGroup(AccessUser accessUser) {
        return manager.createQuery("select g from AccessGroup g JOIN g.accessUsers accessUser where accessUser.id = :i", AccessGroup.class)
                .setParameter("i", accessUser.getId())
                .getResultList();
    }

    public List<AccessGroup> all() {
        return manager.createQuery("select g from AccessGroup g where g.id != 1", AccessGroup.class).getResultList();
    }
}
