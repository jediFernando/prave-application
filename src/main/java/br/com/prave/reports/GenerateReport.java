package br.com.prave.reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class GenerateReport<T>{

    private String fileName;
    private Map<String, Object> parameters;
    private Connection connection;
    private List<T> fileList;

    public GenerateReport(String fileName, Map<String, Object> parameters, Connection connection) {
        this.fileName = fileName;
        this.parameters = parameters;
        this.connection = connection;
    }

    public GenerateReport(String fileName, Map<String, Object> parameters,List<T> list) {
        this.fileName = fileName;
        this.parameters = parameters;
        this.fileList = list;
    }

    public void generatePDFForOutputStream(HttpServletResponse response) throws IOException {

        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(this.fileName, this.parameters, connection);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=RelatorioPrave.pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
            response.getOutputStream().flush();
            response.getOutputStream().close();

        } catch (JRException e) {
            throw new RuntimeException(e);
        }
    }

    public void generatePDFSaleOrder(HttpServletResponse response) throws IOException {

        try {
            JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(fileList);
            JasperPrint jasperPrint = JasperFillManager.fillReport(this.fileName, parameters, ds);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=OrdemDeCompra.pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
            response.getOutputStream().flush();
            response.getOutputStream().close();

        } catch (JRException e) {
            throw new RuntimeException(e);
        }
    }

}