package br.com.prave.controllers;

import br.com.prave.daos.EmployeeDAO;
import br.com.prave.daos.OrderDAO;
import br.com.prave.daos.ProductDAO;
import br.com.prave.daos.ProviderDAO;
import br.com.prave.enums.BuyOrderStatus;
import br.com.prave.enums.ProductStatus;
import br.com.prave.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Controller
@Transactional
public class ProductUniController {

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private ProviderDAO providerDAO;
    @Autowired
    private EmployeeDAO employeeDAO;
    @Autowired
    private OrderDAO orderDAO;

    @GetMapping("/dashboard/stock/purchase-order")
    public ModelAndView ordersList(Product product) {
        List<BuyOrder> buyOrders = orderDAO.all(getCompanyId());
        return new ModelAndView("product-uni").addObject("allOrders", buyOrders);
    }

    @GetMapping("/dashboard/stock/purchase-order/new-order")
    public ModelAndView placeOrder(BuyOrder buyOrder) {

        List<Product> productList = productDAO.all(getCompanyId());
        List<Provider> providerList = providerDAO.allActive(getCompanyId());

        ModelAndView view = new ModelAndView("product-uni-add")
                .addObject("allProducts", productList);

        view.addObject("allProviders", providerList);

        return view;
    }

    @PostMapping("/dashboard/stock/purchase-order/new-order")
    public ModelAndView saveOrder(@Valid BuyOrder buyOrder) {
        Company buyOrderCompany = new Company();
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        buyOrderCompany.setId(employee.getCompany().getId());
        buyOrder.setCompany(buyOrderCompany);
        buyOrder.setBuyOrderStatus(BuyOrderStatus.WAITING);
        orderDAO.save(buyOrder);
        return new ModelAndView("redirect:/dashboard/stock/purchase-order");
    }

    @PostMapping("/dashboard/stock/")
    public ModelAndView refuseOrder(@Valid BuyOrder buyOrder) {
        BuyOrder order = orderDAO.findById(buyOrder.getId());
        orderDAO.remove(order);
        return new ModelAndView("redirect:/dashboard/stock/purchase-order");
    }

    @PostMapping("/dashboard/stock/purchase-order")
    public ModelAndView acceptOrder(@Valid BuyOrder buyOrder) {
        Product product = productDAO.findById(buyOrder.getProduct().getId());
        int productQuantity = product.getQuantity();
        int orderQuantity = buyOrder.getQuantity();
        product.setQuantity(productQuantity + orderQuantity);
        product.setProductStatus(ProductStatus.ACTIVE);
        BuyOrder order = orderDAO.findById(buyOrder.getId());
        order.setBuyOrderStatus(BuyOrderStatus.APPROVED);
        orderDAO.update(order);
        return new ModelAndView("redirect:/dashboard/stock/purchase-order");
    }


    private Integer getCompanyId() {
        Employee employee = new Employee();
        Integer employee_id = employee.getLoggedUser().getId();
        employee.setId(employee_id);
        employee = employeeDAO.findById(employee.getId());
        return employee.getCompany().getId();
    }
}
