package br.com.prave.controllers;

import br.com.prave.conf.JPAConfiguration;
import br.com.prave.daos.EmployeeDAO;
import br.com.prave.daos.OrderDAO;
import br.com.prave.models.BuyOrder;
import br.com.prave.models.Employee;
import br.com.prave.models.Provider;
import br.com.prave.reports.GenerateReport;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.postgresql.core.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ReportController {

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private ApplicationContext context;

    @GetMapping("dashboard/report/provider-report")
    public void generateProviderReport(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            String name = request.getServletContext().getRealPath("/WEB-INF/jasper/RelatorioFornecedores.jasper");
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("comp_id", getCompanyId());
            GenerateReport generator = new GenerateReport(name, parameters, getConnection());
            generator.generatePDFForOutputStream(response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("dashboard/report/employee-report")
    public void generateEmployeesReport(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            // TODO qual o setor de cada funcionario?
            String name = request.getServletContext().getRealPath("/WEB-INF/jasper/RelatorioFuncionarios.jasper");
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("comp_id", getCompanyId());
            GenerateReport generator = new GenerateReport(name, parameters, getConnection());
            generator.generatePDFForOutputStream(response);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @GetMapping("dashboard/report/sales-report")
    public void generateSalesReport(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            String name = request.getServletContext().getRealPath("/WEB-INF/jasper/RelatorioVendas.jasper");
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("comp_id", getCompanyId());
            GenerateReport generator = new GenerateReport(name, parameters, getConnection());
            generator.generatePDFForOutputStream(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("dashboard/report/buy-order-report")
    public void generateBuyOrderReport(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            String name = request.getServletContext().getRealPath("/WEB-INF/jasper/RelatorioOrdemEstoque.jasper");
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("comp_id", getCompanyId());
            GenerateReport generator = new GenerateReport(name, parameters, getConnection());
            generator.generatePDFForOutputStream(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException {
        JPAConfiguration bean = (JPAConfiguration) context.getBean("JPAConfiguration");
        DataSource source = bean.getDataSource();
        return source.getConnection();
    }

    private Integer getCompanyId() {
        Employee employee = new Employee();
        Integer employee_id = employee.getLoggedUser().getId();
        employee.setId(employee_id);
        employee = employeeDAO.findById(employee.getId());
        return employee.getCompany().getId();
    }
}
