package br.com.prave.controllers;

import br.com.prave.daos.CompanyDAO;
import br.com.prave.models.AccessGroup;
import br.com.prave.enums.AccessStatus;
import br.com.prave.models.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@Transactional
public class CompanyController {

    @Autowired
    private CompanyDAO companyDAO;

    @GetMapping("/cadastro")
    public ModelAndView form(Company company){
        return new ModelAndView("/companyForm");
    }

    @PostMapping("/cadastro")
    public ModelAndView save(@Valid Company company, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return form(company);
        }
        List<AccessGroup> accessGroups = new ArrayList<>();

        AccessGroup accessGroup = new AccessGroup();
        accessGroup.setId(1);
        accessGroups.add(accessGroup);

        company.setAccessGroups(accessGroups);
        company.setAccessStatus(AccessStatus.ACTIVE);
        company.setPassword(company.cryptPassword(company.getPassword()));
        companyDAO.save(company);
        return new ModelAndView("redirect:/");
    }
}
