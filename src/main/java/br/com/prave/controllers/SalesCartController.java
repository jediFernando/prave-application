package br.com.prave.controllers;

import br.com.prave.daos.ProductDAO;
import br.com.prave.models.Product;
import br.com.prave.models.SalesCart;
import br.com.prave.models.ShoppingProduct;
import br.com.prave.reports.GenerateReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@Transactional
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class SalesCartController {

    @Autowired
    private SalesCart salesCart;

    @Autowired
    private ProductDAO productDAO;

    @GetMapping("/dashboard/sell/list-cart")
    public ModelAndView listCart(Product product) {
        return new ModelAndView("sell-cart").addObject("allCart", salesCart.getShoppingProducts());
    }

    @PostMapping("/dashboard/sell/add-cart")
    public String addToCart(Product product) {
        salesCart.addToCart(product);
        return "redirect:/dashboard/sell/list-cart";
    }

    @GetMapping("/dashboard/sell/delete-cart/{id}")
    public String deleteFromCart(@PathVariable("id") Integer id) {
        salesCart.deleteProduct(id);
        return "redirect:/dashboard/sell/list-cart";
    }

    @PostMapping("/dashboard/sell/update-cart")
    public String updateCart(Product product) {
        salesCart.updateProduct(product);
        return "redirect:/dashboard/sell/list-cart";
    }


    @GetMapping("/dashboard/sell/order-cart")
    public void generateOrder(HttpServletRequest request, HttpServletResponse response) {
        try {
            String name = request.getServletContext().getRealPath("/WEB-INF/jasper/OrdemCompra.jasper");
            Map<String, Object> params = new HashMap<String, Object>();
            GenerateReport generator = new GenerateReport(name, params, salesCart.getShoppingProducts());
            generator.generatePDFSaleOrder(response);
            salesCart.updateQuantity(productDAO);
            salesCart.getShoppingProducts().clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
