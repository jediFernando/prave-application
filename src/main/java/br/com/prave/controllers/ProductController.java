package br.com.prave.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.prave.daos.*;
import br.com.prave.enums.ProductStatus;
import br.com.prave.enums.WeightType;
import br.com.prave.models.Company;
import br.com.prave.models.Employee;
import br.com.prave.models.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import br.com.prave.models.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@Transactional
public class ProductController {

   @Autowired
   private ProductDAO productDAO;
   @Autowired
   private AccessUserDAO accessUserDAO;

   @ModelAttribute("weightTypes")
   public List<WeightType> populateRoster() {
      return Arrays.asList(WeightType.values());
   }

   @GetMapping("/dashboard/stock/add-product")
   public ModelAndView addProduct(Product product) {

      return new ModelAndView("product-add");
   }

   @RequestMapping(value = "/dashboard/stock/add-product", method = RequestMethod.POST)
   public ModelAndView save(@Valid Product product, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         return addProduct(product);
      }

      if(Integer.parseInt(product.getWeight()) <= 0){
         ObjectError objectError = new FieldError(product.getWeight(),"","Peso deve ser maior que zero.");
         bindingResult.addError(objectError);
         return addProduct(product);
      }

      if(product.getMinimum() < 0){
         ObjectError objectError = new FieldError(product.getMinimum().toString(),"","Quantidade mínima deve ser igual ou maior que zero.");
         bindingResult.addError(objectError);
         return addProduct(product);
      }

      if(product.getPrice() < 0){
         ObjectError objectError = new FieldError(product.getWeight(),"","Preço deve ser igual ou maior que zero.");
         bindingResult.addError(objectError);
         return addProduct(product);
      }

      Company productCompany = new Company();
      Integer userId = product.getUser().getId();
      Employee employee = accessUserDAO.findById(userId);
      productCompany.setId(employee.getCompany().getId());
      product.setCompany(productCompany);
      product.setQuantity(0);
      product.setProductStatus(ProductStatus.UNRELEASED);
      productDAO.save(product);
      return new ModelAndView("redirect:/dashboard/stock");
   }

   @GetMapping("/dashboard/stock/edit/{id}")
   public ModelAndView editProduct(@PathVariable("id") Integer id) {
      ModelAndView view = new ModelAndView("product-update");
      Product product = productDAO.findById(id);
      view.addObject("product", product);
      return view;
   }

   @RequestMapping(value = "/dashboard/stock/edit/{id}", method = RequestMethod.POST)
   public ModelAndView updateProduct(@PathVariable("id") Integer id, @Valid Product product, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         return editProduct(product.getId());
      }

      if(Integer.parseInt(product.getWeight()) <= 0){
         ObjectError objectError = new FieldError(product.getWeight(),"","Peso deve ser maior que zero.");
         bindingResult.addError(objectError);
         return editProduct(product.getId());
      }

      if(product.getMinimum() < 0){
         ObjectError objectError = new FieldError(product.getMinimum().toString(),"","Quantidade mínima deve ser igual ou maior que zero.");
         bindingResult.addError(objectError);
         return editProduct(product.getId());
      }

      if(product.getPrice() < 0){
         ObjectError objectError = new FieldError(product.getWeight(),"","Preço deve ser igual ou maior que zero.");
         bindingResult.addError(objectError);
         return editProduct(product.getId());
      }

      Product updateProduct = productDAO.findById(id);

      Company productCompany = new Company();
      Integer userId = product.getUser().getId();
      Employee employee = accessUserDAO.findById(userId);
      productCompany.setId(employee.getCompany().getId());
      updateProduct.setQuantity(product.getQuantity());
      updateProduct.setCompany(productCompany);
      productDAO.update(updateProduct);
      return new ModelAndView("redirect:/dashboard/stock");
   }
}
