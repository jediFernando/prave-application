package br.com.prave.controllers;

import br.com.prave.daos.EmployeeDAO;
import br.com.prave.daos.ProviderDAO;
import br.com.prave.enums.ProviderStatus;
import br.com.prave.models.AccessGroup;
import br.com.prave.models.Company;
import br.com.prave.models.Employee;
import br.com.prave.models.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Controller
@Transactional
public class ProductProviderController {

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private ProviderDAO providerDAO;

    @GetMapping("/dashboard/stock/provider")
    public ModelAndView providerList(Provider provider) {
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        List<Provider> providers = providerDAO.all(employee.getCompany().getId());
        return new ModelAndView("product-provider").addObject("allProviders", providers);
    }

    @GetMapping("/dashboard/stock/provider/add-provider")
    public ModelAndView addProvider(Provider provider) {
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        List<Provider> providers = providerDAO.all(employee.getCompany().getId());
        return new ModelAndView("product-provider-add").addObject("allProviders", providers);
    }

    @RequestMapping(value = "/dashboard/stock/provider/add-provider", method = RequestMethod.POST)
    public ModelAndView saveNewProvider(@Valid Provider provider, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return providerList(provider);
        }
        Company productCompany = new Company();
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        productCompany.setId(employee.getCompany().getId());
        provider.setProviderStatus(ProviderStatus.valueOf(ProviderStatus.ACTIVE.toString()));
        provider.setCompany(productCompany);
        providerDAO.save(provider);
        return new ModelAndView("redirect:/dashboard/stock/provider");
    }

    @RequestMapping(value = "/dashboard/stock/provider/edit/{id}", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("id") Integer id) {
        ModelAndView view = new ModelAndView("product-provider-update");
        Provider provider = providerDAO.findById(id);
        view.addObject("provider", provider);
        return view;
    }

    @RequestMapping(value = "/dashboard/stock/provider/edit/{id}", method = RequestMethod.POST)
    @CacheEvict(value = "provider", allEntries = true)
    public ModelAndView update(@PathVariable("id") Integer id, @Valid Provider provider, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return providerList(provider);
        }
        Company productCompany = new Company();
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        productCompany.setId(employee.getCompany().getId());
        provider.setCompany(productCompany);
        providerDAO.update(provider);
        return new ModelAndView("redirect:/dashboard/stock/provider");
    }

}
