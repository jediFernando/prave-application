package br.com.prave.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.prave.daos.AccessGroupDAO;
import br.com.prave.daos.CompanyDAO;
import br.com.prave.daos.EmployeeDAO;
import br.com.prave.daos.AccessStatusDAO;
import br.com.prave.enums.AccessStatus;
import br.com.prave.models.AccessGroup;
import br.com.prave.models.Company;
import br.com.prave.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@Transactional
public class EmployeeController {

    @Autowired
    private EmployeeDAO employeeDAO;
    @Autowired
    private CompanyDAO companyDAO;
    @Autowired
    AccessGroupDAO accessGroupDAO;
    @Autowired
    AccessStatusDAO accessStatusDAO;

    @GetMapping("/dashboard/employee/add-employee")
    public ModelAndView addEmployee(Employee employee) {

        List<AccessGroup> accessGroups = accessGroupDAO.all();

        return new ModelAndView("employee-add").addObject("allGroups", accessGroups);
    }

    @RequestMapping(value = "/dashboard/employee/add-employee", method = RequestMethod.POST)
    public ModelAndView save(@Valid Employee employee, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return addEmployee(employee);
        }

        if(employee.getName().matches(".*\\d.*")){
            ObjectError objectError = new FieldError(employee.getName(),"","Nome não pode conter números.");
            bindingResult.addError(objectError);
            return addEmployee(employee);
        }

        Company employeeCompany = new Company();
        Integer companyId = employee.getLoggedUser().getId();
        employeeCompany.setId(companyId);

        employee.setAccessStatus(AccessStatus.ACTIVE);
        employee.setCompany(employeeCompany);
        employee.setPassword(employee.cryptPassword(employee.getPassword()));
        employeeDAO.save(employee);
        return new ModelAndView("redirect:/dashboard/employee");
    }

    @RequestMapping(value = "/dashboard/employee/edit/{id}", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("id") Integer id) {

        List<AccessGroup> accessGroups = accessGroupDAO.all();

        ModelAndView view = new ModelAndView("employee-update")
                .addObject("allGroups", accessGroups);

        Employee employee = employeeDAO.findById(id);

        view.addObject("employee", employee);
        return view;
    }

    @RequestMapping(value = "/dashboard/employee/edit/{id}", method = RequestMethod.POST)
    @CacheEvict(value = "employee", allEntries = true)
    public ModelAndView update(@PathVariable("id") Integer id, @Valid Employee employee, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        boolean update = false;
        boolean anotherError = false;

        for (ObjectError error : bindingResult.getAllErrors()) {
            if (error.getDefaultMessage().contains("Login já existe")) {
                update = true;
                bindingResult.getAllErrors().indexOf(error);
            } else {
                anotherError = true;
            }
        }

        if ((bindingResult.hasErrors() && !update) || anotherError) {
            List<AccessGroup> accessGroups = accessGroupDAO.all();
            ModelAndView view = new ModelAndView("employee-update")
                    .addObject("allGroups", accessGroups);
            return view;
        }

        Company employeeCompany = new Company();
        Integer companyId = employee.getLoggedUser().getId();
        employeeCompany.setId(companyId);
        employee.setCompany(employeeCompany);
        employee.setPassword(employee.cryptPassword(employee.getPassword()));
        employeeDAO.update(employee);
        return new ModelAndView("redirect:/dashboard/employee");
    }

}
