package br.com.prave.controllers;

import br.com.prave.daos.EmployeeDAO;
import br.com.prave.daos.ProductDAO;
import br.com.prave.models.Employee;
import br.com.prave.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class DashboardController {

    @Autowired
    EmployeeDAO employeeDAO;

    @Autowired
    ProductDAO productDAO;

    @GetMapping("/dashboard")
    public ModelAndView dashboard(){
        return new ModelAndView("dashboard");
    }

    @GetMapping("/dashboard/report")
    public ModelAndView report(){
        return new ModelAndView("report");
    }

    @GetMapping("/dashboard/employee")
    public ModelAndView employee(){
        List<Employee> employees = employeeDAO.all(new Employee().getLoggedUser().getId());
        return new ModelAndView("employee").addObject("allEmployees", employees);
    }

    @GetMapping("/dashboard/stock")
    public ModelAndView stock(){
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        List<Product> products = productDAO.all(employee.getCompany().getId());
        return new ModelAndView("product").addObject("allProducts", products);
    }

    @GetMapping("/dashboard/sell")
    public ModelAndView sell(){
        Employee employee = employeeDAO.findById(new Employee().getLoggedUser().getId());
        List<Product> products = productDAO.allQuantity(employee.getCompany().getId());
        return new ModelAndView("sell").addObject("allProducts", products);
    }
}
